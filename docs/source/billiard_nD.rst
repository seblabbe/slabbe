.. nodoctest

Hypercubic billiard subshifts
=============================

.. automodule:: slabbe.billiard_nD
   :members:
   :undoc-members:
   :show-inheritance:
   

