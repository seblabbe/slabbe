.. nodoctest

Random generation of teams for local Ultimate Frisbee league organization
=========================================================================

.. automodule:: slabbe.random_team_creation
   :members:
   :undoc-members:
   :show-inheritance:
   

