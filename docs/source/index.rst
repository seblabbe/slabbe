.. include:: ../../README.rst

Module documentation
====================

This reference manual contains many examples that illustrate the usage of the
slabbe package. The examples are all tested at each release and should produce
exactly the same output as in this manual.

This work is licensed under a `Creative Commons Attribution-Share Alike
3.0 License`__.

__ https://creativecommons.org/licenses/by-sa/3.0/

Dynamical systems
-----------------

.. toctree::
   :maxdepth: 1

   matrix_cocycle
   mult_cont_frac
   diophantine_approx
   lyapunov
   markov_transformation
   matrices
   polyhedron_exchange_transformation
   piecewise_affine_transformation
   substitution_2d
   coding_of_PETs
   graph_directed_IFS
   EkEkstar

Combinatorics
-------------

.. toctree::
   :maxdepth: 1

   joyal_bijection
   bond_percolation
   dyck_3d
   combinat
   graph
   partial_injection
   magic_hexagon
   q_markoff

Combinatorics on words
----------------------

.. toctree::
   :maxdepth: 1

   sturmian_subshift
   kolakoski_word
   bispecial_extension_type
   finite_word
   infinite_word
   language
   word_morphisms
   ostrowski
   beta_numeration_system
   ddim_sturmian_configuration
   billiard_nD

Digital Geometry
----------------

.. toctree::
   :maxdepth: 1

   discrete_subset
   discrete_plane
   billiard
   christoffel_graph
   polyhedron_partition

Tilings
-------

.. toctree::
   :maxdepth: 1

   double_square_tile
   aperiodic_monotile
   cut_and_project_scheme
   wang_tiles
   wang_cubes

Vizualization
-------------

.. toctree::
   :maxdepth: 1

   tikz_picture

Code in research papers
-----------------------

.. toctree::
   :maxdepth: 1

   arXiv_1808_07768
   arXiv_1903_06137
   arXiv_1906_01104

Miscellaneous
-------------

.. toctree::
   :maxdepth: 1

   analyze_sage_build
   ranking_scale
   random_team_creation
   fruit
   write_to_file


Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
