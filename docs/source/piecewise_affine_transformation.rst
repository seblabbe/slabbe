.. nodoctest

Piecewise affine transformations (PATs) and induced transformations
===================================================================

.. automodule:: slabbe.piecewise_affine_transformation
   :members:
   :undoc-members:
   :show-inheritance:
   

