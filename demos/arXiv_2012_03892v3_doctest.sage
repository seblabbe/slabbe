r"""
This file was *autogenerated* from chapter.tex with sagetex.sty
version 2021/10/16 v3.6. It contains the contents of all the
sageexample environments from chapter.tex. You should be able to
doctest this file with "sage -t chapter_doctest.sage".

It is always safe to delete this file; it is not used in typesetting your
document.

Sage commandline, line 399::

sage: version()
'SageMath version ..., Release Date: ...'
sage: import importlib.metadata
sage: importlib.metadata.version("slabbe")
'...'

Sage commandline, line 1104::

sage: from slabbe import Substitution2d
sage: Phi = Substitution2d({0: [[14]], 1: [[13]], 2: [[12],[10]],
....: 3: [[11],[8]], 4: [[14],[7]], 5: [[13],[7]], 6: [[12],[7]],
....: 7: [[12,6]], 8: [[14,3]], 9: [[13,3]], 10: [[12,2]],
....: 11: [[12,6],[10,1]], 12: [[11,6],[8,1]], 13: [[15,5],[9,1]],
....: 14: [[11,4],[8,1]], 15: [[12,2],[7,0]]})

Sage commandline, line 2799::

sage: from slabbe import WangTileSet
sage: tiles = ["DOJO", "DOHL", "JMDP", "DMDK", "HPJP", "HPHN", "HKDP", "BOIO",
....:   "ILEO", "ILCL", "ALIO", "EPIP", "IPIK", "IKBM", "IKAK", "CNIP"]
sage: Z = WangTileSet([tuple(tile) for tile in tiles])

Sage commandline, line 2815::

sage: Z.find_markers(i=2,radius=2,solver="dancing_links")
[[0, 1, 2, 3, 4, 5, 6]]
sage: M = [0, 1, 2, 3, 4, 5, 6]
sage: V,alpha0 = Z.find_substitution(M, i=2, radius=2, side="right",
....:                                solver="dancing_links")

Sage commandline, line 2845::

sage: V.find_markers(i=1,radius=1,solver="dancing_links")
[[0, 1, 2, 7, 8, 9, 10]]
sage: M = [0, 1, 2, 7, 8, 9, 10]
sage: W,alpha1 = V.find_substitution(M, i=1, radius=1, side="right",
....:                                solver="dancing_links")

Sage commandline, line 2873::

sage: W.is_equivalent(Z)
True

Sage commandline, line 2883::

sage: _,vert,horiz,alpha2 = Z.is_equivalent(W, certificate=True)
sage: vert
{'A': 'IJ', 'B': 'IH', 'C': 'BD', 'D': 'I', 'E': 'AD', 'H': 'B', 'I': 'ID', 'J': 'A'}
sage: horiz
{'K': 'PO', 'L': 'M', 'M': 'PL', 'N': 'MO', 'O': 'K', 'P': 'KO'}

Sage commandline, line 2905::

sage: alpha0 * alpha1 * alpha2 == Phi
True

Sage commandline, line 4021::

sage: z = polygen(QQ, "z")
sage: K.<phi> = NumberField(z**2-z-1, "phi", embedding=RR(1.6))
sage: from slabbe import PolyhedronExchangeTransformation as PET
sage: from slabbe.arXiv_1903_06137 import self_similar_19_atoms_partition

Sage commandline, line 4035::

sage: Gamma0 = matrix.column([(1,0), (0,1)])
sage: PU = self_similar_19_atoms_partition()
sage: merge_dict = {0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:6, 8:7, 9:8, 10:9,
....:               11:10, 12:11, 13:11, 14:12, 15:12, 16:13, 17:14, 18:15}
sage: PZ = PU.merge_atoms(merge_dict)
sage: RZe1 = PET.toral_translation(Gamma0, vector((phi^-2,0)))
sage: RZe2 = PET.toral_translation(Gamma0, vector((0,phi^-2)))

Sage commandline, line 4075::

sage: y_ineq = [phi^-1, 0, -1] # y <= phi^-1 (see Polyhedron?)
sage: P1,beta0 = RZe2.induced_partition(y_ineq,PZ,
....:                                   substitution_type="column")
sage: R1e1,_ = RZe1.induced_transformation(y_ineq)
sage: R1e2,_ = RZe2.induced_transformation(y_ineq)

Sage commandline, line 4126::

sage: x_ineq = [phi^-1, -1, 0] # x <= phi^-1 (see Polyhedron?)
sage: P2,beta1 = R1e1.induced_partition(x_ineq, P1, substitution_type="row")
sage: R2e1,_ = R1e1.induced_transformation(x_ineq)
sage: R2e2,_ = R1e2.induced_transformation(x_ineq)

Sage commandline, line 4170::

sage: P2_scaled = (-phi*P2).translate((1,1))

Sage commandline, line 4196::

sage: assert P2_scaled.is_equal_up_to_relabeling(PZ)
sage: beta2 = Substitution2d.from_permutation(PZ.keys_permutation(P2_scaled))

Sage commandline, line 4242::

sage: beta0 * beta1 * beta2 == Phi
True

Sage commandline, line 4529::

sage: image = Phi([[12]], order=4)
sage: image
[[12, 6, 12, 11, 6, 12, 6, 12],
[10, 1, 7, 8, 1, 10, 1, 7],
[14, 3, 13, 12, 6, 14, 3, 13],
[11, 6, 12, 15, 5, 11, 6, 12],
[8, 1, 10, 9, 1, 8, 1, 7],
[12, 6, 14, 12, 6, 12, 2, 13],
[10, 1, 7, 10, 1, 7, 0, 7],
[14, 3, 13, 14, 3, 13, 3, 13]]
sage: seed = [[1,8],[6,12]]  # using Cartesian-like coordinates
sage: image = Phi(seed, order=4)

Sage commandline, line 4553::

sage: XPhi_2x1 = Phi.list_dominoes(direction='horizontal')
sage: sorted(XPhi_2x1)
[[[0], [3]], [[1], [2]], [[1], [3]], [[1], [6]], [[2], [0]], [[2], [4]],
[[3], [6]], [[4], [1]], [[5], [1]], [[6], [1]], [[6], [5]], [[7], [13]],
[[8], [12]], [[9], [11]], [[9], [12]], [[10], [14]], [[11], [8]], [[12],
[7]], [[12], [10]], [[12], [11]], [[12], [15]], [[13], [7]], [[13], [11]],
[[13], [12]], [[14], [7]], [[14], [11]], [[15], [9]]]
sage: XPhi_1x2 = Phi.list_dominoes(direction='vertical')
sage: sorted(XPhi_1x2)
[[[0, 7]], [[1, 7]], [[1, 8]], [[1, 10]], [[2, 13]], [[3, 13]], [[4, 11]],
[[5, 11]], [[6, 12]], [[6, 14]], [[7, 0]], [[7, 8]], [[7, 10]], [[8, 1]],
[[8, 9]], [[9, 1]], [[10, 1]], [[10, 9]], [[11, 4]], [[11, 6]], [[11, 15]],
[[12, 2]], [[12, 6]], [[12, 11]], [[12, 15]], [[13, 3]], [[13, 12]], [[13,
14]], [[14, 3]], [[14, 12]], [[15, 5]]]

Sage commandline, line 4596::

sage: sorted(Phi.list_2x2_factors())
[[[0, 7], [3, 13]], [[1, 7], [2, 13]], [[1, 7], [3, 13]], [[1, 8], [6, 12]], [[1, 10], [6, 14]], [[2, 13], [0, 7]], [[2, 13], [4, 11]], [[3, 13], [6, 12]], [[4, 11], [1, 8]], [[5, 11], [1, 8]], [[6, 12], [1, 7]], [[6, 12], [1, 10]], [[6, 12], [5, 11]], [[6, 14], [1, 7]], [[6, 14], [5, 11]], [[7, 0], [13, 3]], [[7, 8], [13, 12]], [[7, 10], [13, 14]], [[8, 1], [12, 2]], [[8, 1], [12, 6]], [[8, 9], [12, 11]], [[9, 1], [11, 6]], [[9, 1], [12, 6]], [[10, 1], [14, 3]], [[10, 9], [14, 12]], [[11, 4], [8, 1]], [[11, 6], [8, 1]], [[11, 15], [8, 9]], [[12, 2], [7, 0]], [[12, 2], [11, 4]], [[12, 6], [10, 1]], [[12, 6], [15, 5]], [[12, 11], [7, 8]], [[12, 15], [10, 9]], [[13, 3], [11, 6]], [[13, 3], [12, 6]], [[13, 12], [7, 10]], [[13, 12], [11, 15]], [[13, 12], [12, 11]], [[13, 12], [12, 15]], [[13, 14], [12, 11]], [[14, 3], [11, 6]], [[14, 12], [7, 10]], [[14, 12], [11, 15]], [[15, 5], [9, 1]]]

Sage commandline, line 4611::

sage: seeds = sorted(flatten(Phi.prolongable_seeds_list()))
sage: len(seeds)
96
sage: seeds[0]
[ 8 11]
[ 1  4]

Sage commandline, line 4626::

sage: seeds_as_lists_of_lists = [[list(col[::-1]) for col in m.columns()] for m in seeds]
sage: XPhi_2x2 = Phi.list_2x2_factors()
sage: all(seed in XPhi_2x2 for seed in seeds_as_lists_of_lists)
False

Sage commandline, line 4636::

sage: [seed for seed in seeds_as_lists_of_lists if seed in XPhi_2x2]
[[[1, 8], [6, 12]],
[[7, 8], [13, 12]],
[[8, 9], [12, 11]],
[[10, 9], [14, 12]],
[[2, 13], [4, 11]],
[[3, 13], [6, 12]],
[[6, 14], [5, 11]],
[[13, 14], [12, 11]]]

Sage commandline, line 4691::

sage: matrix(Phi).is_primitive()
True

Sage commandline, line 4703::

sage: Gh = Phi.periodic_horizontal_domino_seeds_graph(clean_sources=True)
sage: Gh
Looped digraph on 36 vertices
sage: sorted(Gh.vertices())
[(1, 2), (1, 4), (1, 5), (1, 6), (2, 4), (2, 5), (2, 6), (3, 2), (3, 4), (3, 5), (3, 6), (6, 4), (6, 5), (6, 6), (7, 12), (7, 13), (7, 14), (8, 11), (8, 12), (8, 13), (8, 14), (9, 11), (9, 12), (9, 15), (10, 12), (10, 13), (10, 14), (12, 11), (12, 15), (13, 11), (13, 12), (13, 13), (13, 14), (14, 11), (14, 12), (14, 15)]

Sage commandline, line 4713::

sage: Gv = Phi.periodic_vertical_domino_seeds_graph(clean_sources=True)
sage: Gv
Looped digraph on 36 vertices
sage: sorted(Gv.vertices())
[(1, 7), (1, 8), (1, 9), (1, 10), (2, 13), (2, 14), (3, 13), (3, 14), (4, 11), (4, 12), (5, 11), (5, 12), (5, 15), (6, 11), (6, 12), (6, 13), (6, 14), (7, 7), (7, 8), (7, 9), (7, 10), (8, 8), (8, 9), (10, 8), (10, 9), (11, 11), (11, 15), (12, 11), (12, 12), (12, 15), (13, 11), (13, 12), (13, 13), (13, 14), (14, 11), (14, 12)]

Sage commandline, line 4724::

sage: G = Phi.prolongable_seeds_graph(clean_sources=True)
sage: G
Looped digraph on 96 vertices
sage: sorted(G.vertices())[0]
[ 8 11]
[ 1  4]

Sage commandline, line 4839::

sage: from slabbe import WangTileSet
sage: tiles = ["DOJO", "DOHL", "JMDP", "DMDK", "HPJP", "HPHN", "HKDP", "BOIO",
....:   "ILEO", "ILCL", "ALIO", "EPIP", "IPIK", "IKBM", "IKAK", "CNIP"]
sage: Z = WangTileSet([tuple(tile) for tile in tiles])

Sage commandline, line 4848::

sage: tiling = Z.solver(7,7).solve(solver="dancing_links")
sage: tiling.table()    # Cartesian-like coordinates
[[0, 7, 8, 1, 8, 9, 1],
 [2, 13, 12, 6, 12, 11, 6],
 [0, 7, 10, 1, 7, 8, 1],
 [3, 13, 14, 3, 13, 12, 2],
 [6, 12, 11, 6, 12, 11, 4],
 [1, 7, 8, 1, 7, 8, 1],
 [2, 13, 12, 2, 13, 12, 2]]

Sage commandline, line 4864::

sage: Z_2x1 = [t.table() for t in Z.tilings_with_surrounding(2,1,radius=2, solver="dancing_links")]
sage: sorted(Z_2x1)
[[[0], [3]], [[1], [2]], [[1], [3]], [[1], [6]], [[2], [0]], [[2], [4]], [[3], [6]], [[4], [1]], [[5], [1]], [[6], [1]], [[6], [5]], [[7], [13]], [[8], [12]], [[9], [11]], [[9], [12]], [[10], [14]], [[11], [8]], [[12], [7]], [[12], [10]], [[12], [11]], [[12], [15]], [[13], [7]], [[13], [11]], [[13], [12]], [[14], [7]], [[14], [11]], [[15], [9]]]
sage: Z_1x2 = [t.table() for t in Z.tilings_with_surrounding(1,2,radius=2, solver="dancing_links")]
sage: sorted(Z_1x2)
[[[0, 7]], [[1, 7]], [[1, 8]], [[1, 10]], [[2, 13]], [[3, 13]], [[4, 11]], [[5, 11]], [[6, 12]], [[6, 14]], [[7, 0]], [[7, 8]], [[7, 10]], [[8, 1]], [[8, 9]], [[9, 1]], [[10, 1]], [[10, 9]], [[11, 4]], [[11, 6]], [[11, 15]], [[12, 2]], [[12, 6]], [[12, 11]], [[12, 15]], [[13, 3]], [[13, 12]], [[13, 14]], [[14, 3]], [[14, 12]], [[15, 5]]]

Sage commandline, line 4881::

sage: Z.find_markers(i=2, radius=2, solver="dancing_links")
[[0, 1, 2, 3, 4, 5, 6]]

Sage commandline, line 4891::

sage: M = [0, 1, 2, 3, 4, 5, 6]
sage: dominoes_M = sorted((a,b) for [[a,b]] in Z_1x2 if b in M)
sage: from slabbe.wang_tiles import fusion
sage: new_tiles = [fusion(Z[a],Z[b],2) for (a,b) in dominoes_M]
sage: new_tiles
[('BD', 'O', 'IJ', 'O'), ('ID', 'O', 'EH', 'O'), ('ID', 'O', 'CH', 'L'), ('AD', 'O', 'IH', 'O'), ('EH', 'P', 'IJ', 'P'), ('EH', 'K', 'ID', 'P'), ('IJ', 'M', 'ID', 'K'), ('IH', 'K', 'ID', 'K'), ('ID', 'M', 'BD', 'M'), ('ID', 'M', 'AD', 'K'), ('CH', 'P', 'IH', 'P')]
sage: tikz = WangTileSet(new_tiles).tikz(id=None)

Sage commandline, line 4910::

sage: M = [0, 1, 2, 3, 4, 5, 6]
sage: V,alpha0 = Z.find_substitution(M=M,i=2,radius=2,solver="dancing_links")
sage: V
Wang tile set of cardinality 18

Sage commandline, line 4952::

sage: sorted(XPhi_2x1) == sorted(Z_2x1)
True
sage: sorted(XPhi_1x2) == sorted(Z_1x2)
True

Sage commandline, line 4966::

sage: tilings = Z.tilings_with_surrounding(2,2,radius=2, solver="dancing_links")
sage: Z_2x2 = [tiling.table() for tiling in tilings]
sage: len(Z_2x2)
45
sage: len(XPhi_2x2)
45
sage: sorted(XPhi_2x2) == sorted(Z_2x2)
True

Sage commandline, line 4987::

sage: from slabbe.arXiv_1903_06137 import self_similar_19_atoms_partition
sage: PU = self_similar_19_atoms_partition()
sage: merge_dict = {0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:6, 8:7, 9:8, 10:9,
....:               11:10, 12:11, 13:11, 14:12, 15:12, 16:13, 17:14, 18:15}
sage: PZ = PU.merge_atoms(merge_dict)
sage: graphics_array([PU.plot(), PZ.plot()])
Graphics Array of size 1 x 2

Sage commandline, line 5001::

sage: z = polygen(QQ, "z")
sage: K.<phi> = NumberField(z**2-z-1, "phi", embedding=RR(1.6))
sage: from slabbe import PolyhedronExchangeTransformation as PET
sage: Gamma0 = matrix.column([(1,0), (0,1)])
sage: RZe1 = PET.toral_translation(Gamma0, vector((phi^-2,0)))
sage: RZe2 = PET.toral_translation(Gamma0, vector((0,phi^-2)))
sage: from slabbe.arXiv_1903_06137 import self_similar_19_atoms_partition
sage: PU = self_similar_19_atoms_partition()
sage: merge_dict = {0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:6, 8:7, 9:8, 10:9,
....:               11:10, 12:11, 13:11, 14:12, 15:12, 16:13, 17:14, 18:15}
sage: PZ = PU.merge_atoms(merge_dict)
sage: from slabbe import PETsCoding
sage: X_PZ_RZ = PETsCoding((RZe1,RZe2), PZ)
sage: pattern = X_PZ_RZ.pattern((.1357+1/phi, .2938+1/phi), (8,10))
sage: m8_10 = matrix.column(col[::-1] for col in pattern)

Sage commandline, line 5045::

sage: from slabbe import PolyhedronExchangeTransformation as PET
sage: lattice_base = matrix.column([(1,0), (0,1)])
sage: translation = vector((1/3, 1/2))
sage: T = PET.toral_translation(lattice_base, translation)
sage: ieq = [1, -1, -1]     # inequality 0 <= 1 - x - y, that is, x + y <= 1
sage: induced_map,substitution = T.induced_transformation(ieq)
sage: substitution
{0: [0],
 1: [1],
 2: [2],
 3: [0, 1],
 4: [0, 3],
 5: [0, 1, 2],
 6: [0, 1, 2, 1],
 7: [0, 1, 2, 1, 0, 3]}
sage: induced_map.partition().plot()
Graphics object consisting of 53 graphics primitives
sage: induced_map.image_partition().plot()
Graphics object consisting of 53 graphics primitives

Sage commandline, line 5084::

sage: z = polygen(QQ, "z")
sage: K.<phi> = NumberField(z**2-z-1, "phi", embedding=RR(1.6))
sage: from slabbe import PolyhedronExchangeTransformation as PET
sage: Gamma0 = matrix.column([(1,0), (0,1)])
sage: RZe1 = PET.toral_translation(Gamma0, vector((phi^-2,0)))
sage: RZe2 = PET.toral_translation(Gamma0, vector((0,phi^-2)))
sage: y_ineq = [phi^-1, 0, -1] # y <= phi^-1: the window W_0
sage: R1e1,_ = RZe1.induced_transformation(y_ineq)
sage: R1e2,_ = RZe2.induced_transformation(y_ineq)
sage: R1e1
Polyhedron Exchange Transformation of
Polyhedron partition of 2 atoms with 2 letters
with translations {0: (-phi + 2, 0), 1: (-phi + 1, 0)}
sage: R1e2
Polyhedron Exchange Transformation of
Polyhedron partition of 2 atoms with 2 letters
with translations {0: (0, -phi + 2), 1: (0, -2*phi + 3)}

Sage commandline, line 5118::

sage: from slabbe import PolyhedronExchangeTransformation as PET
sage: from slabbe import PolyhedronPartition
sage: lattice_base = matrix.column([(1,0), (0,1)])
sage: translation = vector((1/3, 1/2))
sage: T = PET.toral_translation(lattice_base, translation)
sage: square = polytopes.hypercube(2, intervals='zero_one')
sage: P = PolyhedronPartition([square]).refine_by_hyperplane([0,1,-1])
sage: ieq = [1, -1, -1]     # inequality 0 <= 1 - x - y, that is, x + y <= 1
sage: induced_partition,substitution = T.induced_partition(ieq, P)
sage: substitution
{0: [0],
 1: [1],
 2: [0, 0],
 3: [0, 1],
 4: [1, 1],
 5: [0, 0, 0],
 6: [0, 1, 0],
 7: [1, 1, 0],
 8: [0, 1, 0, 1],
 9: [1, 1, 0, 1],
 10: [1, 1, 0, 1, 0, 0],
 11: [1, 1, 0, 1, 0, 1]}

Sage commandline, line 5155::

sage: z = polygen(QQ, "z")
sage: K.<phi> = NumberField(z**2-z-1, "phi", embedding=RR(1.6))
sage: from slabbe import PolyhedronExchangeTransformation as PET
sage: Gamma0 = matrix.column([(1,0), (0,1)])
sage: RZe1 = PET.toral_translation(Gamma0, vector((phi^-2,0)))
sage: RZe2 = PET.toral_translation(Gamma0, vector((0,phi^-2)))
sage: from slabbe.arXiv_1903_06137 import self_similar_19_atoms_partition
sage: PU = self_similar_19_atoms_partition()
sage: merge_dict = {0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:6, 8:7, 9:8, 10:9,
....:               11:10, 12:11, 13:11, 14:12, 15:12, 16:13, 17:14, 18:15}
sage: PZ = PU.merge_atoms(merge_dict)
sage: from slabbe import PETsCoding
sage: X_PZ_RZ = PETsCoding((RZe1,RZe2), PZ)
sage: _,d22 = X_PZ_RZ.partition_for_patterns((2,2))
sage: XPZRZ_2x2 = [[list(col) for col in v] for v in d22.values()]
sage: XPZRZ_2x2
[[[0, 7], [3, 13]], [[1, 7], [2, 13]], [[1, 7], [3, 13]], [[1, 8], [6, 12]], [[1, 10], [6, 14]], [[2, 13], [0, 7]], [[2, 13], [4, 11]], [[3, 13], [6, 12]], [[4, 11], [1, 8]], [[5, 11], [1, 8]], [[6, 12], [1, 7]], [[6, 12], [1, 10]], [[6, 12], [5, 11]], [[6, 14], [1, 7]], [[6, 14], [5, 11]], [[7, 0], [13, 3]], [[7, 8], [13, 12]], [[7, 10], [13, 14]], [[8, 1], [12, 2]], [[8, 1], [12, 6]], [[8, 9], [12, 11]], [[9, 1], [11, 6]], [[9, 1], [12, 6]], [[10, 1], [14, 3]], [[10, 9], [14, 12]], [[11, 4], [8, 1]], [[11, 6], [8, 1]], [[11, 15], [8, 9]], [[12, 2], [7, 0]], [[12, 2], [11, 4]], [[12, 6], [10, 1]], [[12, 6], [15, 5]], [[12, 11], [7, 8]], [[12, 15], [10, 9]], [[13, 3], [11, 6]], [[13, 3], [12, 6]], [[13, 12], [7, 10]], [[13, 12], [11, 15]], [[13, 12], [12, 11]], [[13, 12], [12, 15]], [[13, 14], [12, 11]], [[14, 3], [11, 6]], [[14, 12], [7, 10]], [[14, 12], [11, 15]], [[15, 5], [9, 1]]]
sage: _,d12 = X_PZ_RZ.partition_for_patterns((1,2))
sage: XPZRZ_1x2 = [[list(col) for col in v] for v in d12.values()]
sage: XPZRZ_1x2
[[[0, 7]], [[1, 7]], [[1, 8]], [[1, 10]], [[2, 13]], [[3, 13]], [[4, 11]], [[5, 11]], [[6, 12]], [[6, 14]], [[7, 0]], [[7, 8]], [[7, 10]], [[8, 1]], [[8, 9]], [[9, 1]], [[10, 1]], [[10, 9]], [[11, 4]], [[11, 6]], [[11, 15]], [[12, 2]], [[12, 6]], [[12, 11]], [[12, 15]], [[13, 3]], [[13, 12]], [[13, 14]], [[14, 3]], [[14, 12]], [[15, 5]]]
sage: _,d21 = X_PZ_RZ.partition_for_patterns((2,1))
sage: XPZRZ_2x1 = [[list(col) for col in v] for v in d21.values()]
sage: XPZRZ_2x1
[[[0], [3]], [[1], [2]], [[1], [3]], [[1], [6]], [[2], [0]], [[2], [4]], [[3], [6]], [[4], [1]], [[5], [1]], [[6], [1]], [[6], [5]], [[7], [13]], [[8], [12]], [[9], [11]], [[9], [12]], [[10], [14]], [[11], [8]], [[12], [7]], [[12], [10]], [[12], [11]], [[12], [15]], [[13], [7]], [[13], [11]], [[13], [12]], [[14], [7]], [[14], [11]], [[15], [9]]]

Sage commandline, line 5207::

sage: sorted(XPhi_2x1) == sorted(XPZRZ_2x1)
True
sage: sorted(XPhi_1x2) == sorted(XPZRZ_1x2)
True
sage: sorted(XPhi_2x2) == sorted(XPZRZ_2x2)
True

"""
